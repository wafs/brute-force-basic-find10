'''
"Do these four numbers make 10 in any combination?"
 
- Positives :
        - Now supports any order!
        - Using set to store numbers instead of using list lookup
        - Supports operation order thanks to eval()
        - It's brute force (kekekek)

- Todo : 
        - Remove the "duh" spam from addition and multiplication, such as "8 + 4 - 2 / 1"  ,  "4 + 8 - 2 / 1"
'''
from __future__ import division
from itertools import product, permutations


op = "+-/*"
 
def isTen(a,b,c,d):
        '''
        Tries to make 10 using all possible combinations of numbers and operators
        '''
        numString = ''.join(str(x) for x in (a,b,c,d))
 
        result = None
        
        # All posibilities of number order, SET skips repeats in case of multiple of same digits in number (eg :5501)
        numSet = set(permutations(numString))
        plusCounter = 0

        print "### Trying %s ###" % numString
        print "###################\n"

        # All posibilities of 3x op, including self repeating (eg + + +)
        for i in product(op, repeat=3): 
                for j in numSet:
                        try:
                                eq = j[0] + i[0] + j[1] + i[1] + j[2] + i[2] + j[3]

                                if plusCounter > 0 and (eq).count('+') is 3 :
                                        continue
                                elif plusCounter is 0:
                                        plusCounter = 1

 
                                if eval(eq) == 10 or eval(eq) == 10.0:
                                        result = 10
                                        print "10 found! >>> %s" % (' '.join(str(x) for x in eq))
                                        
         
                        except ZeroDivisionError:
                                continue
        if result != 10:
                print "10 not found in number combination of  %s" % ' '.join(numString)

        print


 # TESTING #
 ###########

'''
comment or uncomment as you please 


isTen(1,2,4,8) # Success
isTen(4,3,2,1) # Success
isTen(7,2,1,4) # Success
isTen(6,4,6,4) # Failure 
isTen(6,6,4,4) # Failure - same integers as before, order shuffled, makes no difference
isTen(1,1,1,1) # Failure 
isTen(0,3,4,1) # Failure 
isTen(0,0,5,5) # Success - ZeroDivisionError goes to the next iteration
isTen(7,2,1,0)
isTen(0,0,0,1)
'''